import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [`
    input {
      padding: 10px;
      margin-bottom: 5px;
      width: 100%;
      max-width: 400px;
      font-size: 1.2rem;
      border: 1px solid #378BA4;
      border-radius: 5px;
      outline: none;
    }
  `]
})
export class InputComponent implements OnInit {
  @Input() searchCategoryName: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
