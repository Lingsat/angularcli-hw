import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styles: [`
    button {
      display: block;
      margin-left: auto;
      margin-bottom: 20px;
      padding: 10px 20px;
      background-color: #378BA4;
      color: #fff;
      font-size: 1rem;
      border: none;
      border-radius: 5px;
      outline: none;
      cursor: pointer;
    }
    button:hover {
      background-color: #81BECE;
    }
  `]
})
export class ButtonComponent implements OnInit {
  @Input() buttonName: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
