import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsModule } from './products/products.module';
import { SharedModule } from '../shared/shared.module';

import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { ProductComponent } from './products/product/product.component';

@NgModule({
  declarations: [
    ProductsComponent,
    CartComponent,
    ProductComponent
  ],
  imports: [
    CommonModule,
    ProductsModule,
    SharedModule
  ],
  exports: [
    ProductsComponent,
    CartComponent
  ]
})
export class FeaturesModule { }
